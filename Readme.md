# Python 2.7 docker image

This image is base on a linux image with python 2.7. Also includes aome required libraries to
successfully run python MySQL.

You can pull it with

```bash
docker pull edraobdu/python:2.7
```

To build the image from source you can do
```bash
docker build -t <username>/<image>:<tag> .
```

Changes can be also requested via Pull Requests to this repository
