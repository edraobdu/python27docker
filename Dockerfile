# Python 2.7 container with the required libraries to run django 1.8 and python mysql
FROM python:2.7.18-slim-stretch

RUN apt-get update

RUN apt-get install -y build-essential libsqlite3-dev zlib1g-dev \
    libgdbm-dev libbz2-dev libssl-dev libdb-dev python-dev default-libmysqlclient-dev
